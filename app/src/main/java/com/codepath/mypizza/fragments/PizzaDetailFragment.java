package com.codepath.mypizza.fragments;

import android.app.AppOpsManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.codepath.mypizza.MainActivity;
import com.codepath.mypizza.R;
import com.codepath.mypizza.data.Pizza;

/**
 * Created by Shyam Rokde on 8/5/16.
 */
public class PizzaDetailFragment extends Fragment {
    int position = 0;
    TextView tvTitle;
    TextView tvDetails;
    private int animationId = R.id.alpha;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            // Get back arguments
            if (getArguments() != null) {
                position = getArguments().getInt("position", 0);
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {

        // Inflate the xml file for the fragment
        return inflater.inflate(R.layout.fragment_pizza_detail, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvDetails = (TextView) view.findViewById(R.id.tvDetails);

        // update view
        tvTitle.setText(Pizza.pizzaMenu[position]);
        tvDetails.setText(Pizza.pizzaDetails[position]);

        int id = ((MainActivity) getActivity()).getAnimationId();
        Animation animation = AnimationUtils.loadAnimation(getContext(), id);
        animation.setRepeatCount(Animation.INFINITE);
        tvTitle.setAnimation(animation);
    }

    // Activity is calling this to update view on Fragment
    public void updateView(int position) {
        tvTitle.setText(Pizza.pizzaMenu[position]);
        tvDetails.setText(Pizza.pizzaDetails[position]);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.alpha).setVisible(false);
        menu.findItem(R.id.rotation).setVisible(false);
        menu.findItem(R.id.scale).setVisible(false);
        menu.findItem(R.id.translation).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

}
